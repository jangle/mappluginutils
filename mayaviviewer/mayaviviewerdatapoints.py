'''
MAP Client, a program to generate detailed musculoskeletal models for OpenSim.
    Copyright (C) 2012  University of Auckland
    
This file is part of MAP Client. (http://launchpad.net/mapclient)

    MAP Client is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MAP Client is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MAP Client.  If not, see <http://www.gnu.org/licenses/>..
'''

from mappluginutils.mayaviviewer import MayaviViewerSceneObject, MayaviViewerObject, colours
import numpy as np

class MayaviViewerDataPointsSceneObject(MayaviViewerSceneObject):

    typeName = 'datapoints'

    def __init__(self, name, points):
        self.name = name
        self.points = points

    def setVisibility(self, visible):
        self.points.visible = visible

    def remove(self):
        self.points.remove()
        self.points = None

class MayaviViewerDataPoints(MayaviViewerObject):

    typeName = 'datapoints'

    def __init__(self, name, coords, scalars=None, renderArgs=None):
        self.name = name
        self.coords = coords

        self.scalarName = 'None'
        if scalars==None:
            self.scalars = {}
        else:
            self.scalars = scalars
        
        if renderArgs==None:
            self.renderArgs = {}
        else:
            self.renderArgs = renderArgs

        self.sceneObject = None
        self.defaultColour = colours['bone']
        if 'color' not in self.renderArgs.keys():
            self.renderArgs['color'] = self.defaultColour

    def setScalarSelection(self, scalarName):
        self.scalarName = scalarName

    def setVisibility(self, visible):
        self.sceneObject.setVisibility(visible)

    def remove(self):
        self.sceneObject.remove()
        self.sceneObject = None

    def draw(self, scene):
        scene.disable_render = True
        d = self.coords
        s = self.scalars.get(self.scalarName)
        renderArgs = self.renderArgs
        if s!=None:
            self.sceneObject = MayaviViewerDataPointsSceneObject(self.name,\
                                scene.mlab.points3d( d[:,0], d[:,1], d[:,2], s, **renderArgs))
        else:
            self.sceneObject = MayaviViewerDataPointsSceneObject(self.name,\
                                scene.mlab.points3d( d[:,0], d[:,1], d[:,2], **renderArgs))
        
        scene.disable_render = False
        return self.sceneObject

    def updateGeometry( self, coords, scene ):
        
        if self.sceneObject==None:
            self.coords = coords
            self.draw(scene)
        else: 
            self.sceneObject.points.mlab_source.set(x=coords[:,0], y=coords[:,1], z=coords[:,2])
            self.coords = coords

    def updateScalar(self, scalarName, scene):
        self.setScalarSelection(scalarName)
        if self.sceneObject==None:
            self.draw(scene)
        else: 
            d = self.coords
            s = self.scalars.get(self.scalarName)
            renderArgs = self.renderArgs
            if s!=None:
                self.sceneObject.points.actor.mapper.scalar_visibility=True
                self.sceneObject.points.mlab_source.reset( x=d[:,0], y=d[:,1], z=d[:,2], s=s )
            else:
                if 'color' not in renderArgs:
                    color = self.defaultColour
                else:
                    color = renderArgs['color']
                    
                self.sceneObject.points.actor.mapper.scalar_visibility=False
                self.sceneObject.points.actor.property.specular_color = color
                self.sceneObject.points.actor.property.diffuse_color = color
                self.sceneObject.points.actor.property.ambient_color = color
                self.sceneObject.points.actor.property.color = color
                self.sceneObject.points.mlab_source.reset( x=d[:,0], y=d[:,1], z=d[:,2] )