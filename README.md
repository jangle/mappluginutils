Library of tools for my MAP Client plugins.

Dependencies:
fieldwork - https://bitbucket.org/jangle/fieldwork
gias - https://bitbucket.org/jangle/gias
mayavi - http://code.enthought.com/projects/mayavi/
scipy - http://www.scipy.org/